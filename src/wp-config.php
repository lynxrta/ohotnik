<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** Имя пользователя MySQL */
define( 'DB_USER', getenv('DB_USER') );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** Имя сервера MySQL */
define( 'DB_HOST', getenv('DB_HOST') );

define( 'WP_HOME', getenv('WP_HOME') );
define( 'WP_SITEURL', getenv('WP_SITEURL') );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}Ah>HMmIbLd(?Iu617|a)&wU6S 5%t3/#:yGB.2WV~3y*;#|f=$uDt!pshE=;{%t' );
define( 'SECURE_AUTH_KEY',  '3^?u=I*GRi!d/}U4tNW86k6ZxzglrO*P>FHros5[QT7qlF&-1Kf87o9$cI|J(yqT' );
define( 'LOGGED_IN_KEY',    '}Lua0MBd]mFQ],N}NEN S4{on#?bf,S`lx{fd38n^a;?]|fiNw9nrW]OC:J^57UB' );
define( 'NONCE_KEY',        'yGVG`c%.i|PAt<vYoC?,KYT08[b]a<9o|I:r#Nf(2@)Xv/cRl}[%HYvLbiWa!xYE' );
define( 'AUTH_SALT',        '$teA8Z)}_B8g>j(Hal|Vi-ZyCz4M~XOA?[4}VmRyVH(Kq#dX_wb(spwSa6.NT%z{' );
define( 'SECURE_AUTH_SALT', 'bX_BFxeW67+z4Du uPRR@weNLxqjsk=ic[_AVQX+Spr/+2*Mj6YnSpiY1a5y:Zi8' );
define( 'LOGGED_IN_SALT',   'F$ED7:&+A(kzOt2>~q}v{&=zVpNjR=EH7WpRFk(Egrc~ECw;;m3WY!T~mc,>L=q1' );
define( 'NONCE_SALT',       'rz4HhSTKC[{:W5*azC#jpRoQaqh(,wjd <9,IfbNdc~vbD(@[qmXur8xB*}>C}~$' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
remove_action( 'template_redirect', 'redirect_canonical' );
