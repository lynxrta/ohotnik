<?php get_header( sailfish_template_base() ); ?>

	<?php sailfish_site_breadcrumbs(); ?>

	<div <?php sailfish_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php sailfish_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include sailfish_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- .container -->

<?php get_footer( sailfish_template_base() ); ?>
