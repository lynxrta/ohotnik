<?php
/**
 * Posts module title template
 *
 * @package sailfish
 */
?>
<div class="sailfishposts_title_group">
	<?php echo $this->html( $this->_var( 'super_title' ), '<h5 class="sailfishposts_supertitle">%s</h5>' ); ?>
	<?php echo $this->html( $this->_var( 'title' ), '<h3 class="sailfishposts_title">%s</h3>' ); ?>
	<?php echo $this->html( $this->_var( 'subtitle' ), '<h6 class="sailfishposts_subtitle">%s</h6>' ); ?>
	<?php echo $this->esc_switcher( 'title_delimiter', '<div class="sailfishposts_title_divider"></div>' ); ?>
</div>
