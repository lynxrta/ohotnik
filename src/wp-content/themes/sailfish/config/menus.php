<?php
/**
 * Menus configuration.
 *
 * @package sailfish
 */

add_action( 'after_setup_theme', 'sailfish_register_menus', 5 );
/**
 * Register menus.
 */
function sailfish_register_menus() {

	register_nav_menus( array(
		'top'          => esc_html__( 'Top', 'sailfish' ),
		'main'         => esc_html__( 'Main', 'sailfish' ),
		'main_landing' => esc_html__( 'Landing Main', 'sailfish' ),
		'footer'       => esc_html__( 'Footer', 'sailfish' ),
		'social'       => esc_html__( 'Social', 'sailfish' ),
	) );
}
