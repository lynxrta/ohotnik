<?php
/**
 * Template part for displaying post meta in Blog module
 *
 * @package sailfish
 */
if ( ! $module->is_meta_visible() ) {
	return;
}
?>
<div class="tm_pb_post_meta entry-meta"><?php

	if ( 'on' === $module->_var( 'show_author' ) ) {
		echo tm_get_safe_localization(
				sprintf(
						'<span class="author vcard posted-by material-icons-face  material-icons-ico">%1$s %2$s</span>',
						esc_html__( 'by ', 'sailfish' ), tm_pb_get_the_author_posts_link()
				)
		);
	}

	if ( 'on' === $module->_var( 'show_date' ) ) {
		echo tm_get_safe_localization(
			sprintf(
				'<span class="published material-icons-schedule material-icons-ico">%s</span>',
				esc_html( get_the_date( $module->_var( 'meta_date' ) ) )
			)
		);
	}

	if ( 'on' === $module->_var( 'show_comments' ) ) {
		printf( '<span class="material-icons-chat_bubble_outline material-icons-ico">%s</span>',
				esc_html( _nx( '1 Comment',  number_format_i18n( get_comments_number() ) . ' Comments', get_comments_number(), 'number of comments', 'sailfish' ) )
		);
	}

	if ( 'on' === $module->_var( 'show_categories' ) ) {
		echo get_the_category_list( ', ' );
	}

?></div>
