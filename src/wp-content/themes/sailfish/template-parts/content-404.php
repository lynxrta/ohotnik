<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sailfish
 */
?>
<section class="error-404 not-found">
	<header class="page-header">
		<h1 class="page-title screen-reader-text"><?php esc_html_e( '404', 'sailfish' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<div>

			<h1><?php esc_html_e( '404', 'sailfish' ); ?></h1>
			<h3><?php esc_html_e( 'We&#39;ve encountered an error and we&#39;re working on it!', 'sailfish' ); ?></h3>
			<p><?php esc_html_e( 'While we&#39;re checking which brick in the wall was a cause, please try refreshing this page again!', 'sailfish' ); ?></p>

		</div>
		<p><a class="btn btn-secondary" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Go home', 'sailfish' ); ?></a></p>

	</div><!-- .page-content -->
</section><!-- .error-404 -->
