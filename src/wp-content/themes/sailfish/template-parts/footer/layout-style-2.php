<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package sailfish
 */

?>
<div class="footer-container <?php echo sailfish_get_invert_class_customize_option( 'footer_bg' ); ?>">
	<div class="site-info container">
		<?php
			sailfish_footer_logo();
			sailfish_footer_menu();
			sailfish_contact_block( 'footer' );
			sailfish_social_list( 'footer' );
			sailfish_footer_copyright();
		?>
	</div><!-- .site-info -->
</div><!-- .container -->
