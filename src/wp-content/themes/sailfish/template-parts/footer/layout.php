<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package sailfish
 */

$footer_contact_block_visibility = get_theme_mod( 'footer_contact_block_visibility', sailfish_theme()->customizer->get_default( 'footer_contact_block_visibility' ) );
?>

<div class="footer-container <?php echo sailfish_get_invert_class_customize_option( 'footer_bg' ); ?>">
	<div class="site-info container">
		<div class="site-info-wrap">
			<?php sailfish_footer_logo(); ?>
			<?php sailfish_footer_menu(); ?>

			<?php if ( $footer_contact_block_visibility ) : ?>
			<div class="site-info__bottom">
			<?php endif; ?>
				<?php sailfish_footer_copyright(); ?>
				<?php sailfish_contact_block( 'footer' ); ?>
			<?php if ( $footer_contact_block_visibility ) : ?>
			</div>
			<?php endif; ?>

			<?php sailfish_social_list( 'footer' ); ?>
		</div>

	</div><!-- .site-info -->
</div><!-- .container -->
