<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sailfish
 */
?>
<div class="header-container_wrap container">
	<div class="header-container__flex">
		<div class="site-branding">
			<?php sailfish_header_logo() ?>
			<?php sailfish_site_description(); ?>
		</div>

		<?php sailfish_main_menu(); ?>
		<?php sailfish_header_btn(); ?>

	</div>
</div>
