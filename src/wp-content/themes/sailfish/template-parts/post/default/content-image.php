<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sailfish
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card' ); ?>>

	<?php $utility       = sailfish_utility()->utility;
	$blog_featured_image = get_theme_mod( 'blog_featured_image', sailfish_theme()->customizer->get_default( 'blog_featured_image' ) );
	$size                = sailfish_post_thumbnail_size( array( 'class_prefix' => 'post-thumbnail--' ) );
	?>

	<?php if ( 'small' === $blog_featured_image ) : ?>
		<div class="post-format-wrap">
			<?php do_action( 'cherry_post_format_image', array( 'size' => $size['size'] ) ); ?>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="post-list__item-content">
		<?php if ( 'small' === $blog_featured_image ) :
			get_template_part( 'template-parts/content-entry-meta-loop' );
		endif; ?>

		<?php if ( 'small' !== $blog_featured_image ) : ?>
			<div class="post-format-wrap">
				<?php do_action( 'cherry_post_format_image', array( 'size' => $size['size'] ) ); ?>
			</div><!-- .post-thumbnail -->
		<?php endif; ?>

		<?php if ( 'small' !== $blog_featured_image ) :
			get_template_part( 'template-parts/content-entry-meta-loop' );
		endif; ?>

		<header class="entry-header">
			<?php sailfish_sticky_label(); ?>

			<?php $title_html = ( is_single() ) ? '<h3 %1$s>%4$s</h3>' : '<h4 %1$s><a href="%2$s" rel="bookmark">%4$s</a></h4>';

			$utility->attributes->get_title( array(
					'class' => 'entry-title',
					'html'  => $title_html,
					'echo'  => true,
			) );
			?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php $blog_content = get_theme_mod( 'blog_posts_content', sailfish_theme()->customizer->get_default( 'blog_posts_content' ) );
			$length             = ( 'full' === $blog_content ) ? -1 : 55;
			$content_visible    = ( 'none' !== $blog_content ) ? true : false;
			$content_type       = ( 'full' !== $blog_content ) ? 'post_excerpt' : 'post_content';

			$utility->attributes->get_content( array(
				'visible'      => $content_visible,
				'length'       => $length,
				'content_type' => $content_type,
				'echo'         => true,
			) );
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">

			<?php $tags_visible = sailfish_is_meta_visible( 'blog_post_tags', 'loop' );

			$utility->meta_data->get_terms( array(
					'visible'   => $tags_visible,
					'type'      => 'post_tag',
					'delimiter' => ', ',
					'before'    => '<span class="post__tags">',
					'after'     => '</span>',
					'echo'      => true,
			) );
			?>

			<?php sailfish_share_buttons( 'loop' ); ?>
		</footer>
		<!-- .entry-footer -->
		<div>
			<?php $btn_text = get_theme_mod( 'blog_read_more_text', sailfish_theme()->customizer->get_default( 'blog_read_more_text' ) );
			$btn_visible    = $btn_text ? true : false;

			$utility->attributes->get_button( array(
					'visible' => $btn_visible,
					'class'   => 'link',
					'text'    => $btn_text,
					'icon'    => '<i class="linearicon linearicon-arrow-right"></i>',
					'html'    => '<a href="%1$s" %3$s><span class="link__text">%4$s</span>%5$s</a>',
					'echo'    => true,
			) );
			?>
		</div>
	</div><!-- .post-list__item-content -->

</article><!-- #post-## -->
