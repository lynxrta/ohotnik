<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sailfish
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card' ); ?>>

	<?php $utility = sailfish_utility()->utility;
	$invert_class  = ( 'skin2' !== get_theme_mod( 'skin_style', sailfish_theme()->customizer->get_default( 'skin_style' ) ) ) ? 'invert invert_primary' : '';
	$blog_featured_image = get_theme_mod( 'blog_featured_image', sailfish_theme()->customizer->get_default( 'blog_featured_image' ) );
	?>

	<?php if ( 'small' !== $blog_featured_image ) :
		get_template_part( 'template-parts/content-entry-meta-loop' );
	endif; ?>

	<div class="post-list__item-content">
		<div class="post-featured-content <?php echo $invert_class ?>">
			<?php $title_html = ( is_single() ) ? '<h3 %1$s>%4$s</h3>' : '<h4 %1$s><a href="%2$s" rel="bookmark">%4$s</a></h4>';

			$utility->attributes->get_title( array(
				'class' => 'entry-title',
				'html'  => $title_html,
				'echo'  => true,
			) );
			?>

			<?php do_action( 'cherry_post_format_link', array( 'render' => true ) ); ?>
		</div><!-- .post-featured-content -->
		<footer class="entry-footer">

			<?php $tags_visible = sailfish_is_meta_visible( 'blog_post_tags', 'loop' );

			$utility->meta_data->get_terms( array(
					'visible'   => $tags_visible,
					'type'      => 'post_tag',
					'delimiter' => ' ',
					'before'    => '<span class="post__tags">',
					'after'     => '</span>',
					'echo'      => true,
			) );
			?>

			<?php sailfish_share_buttons( 'loop' ); ?>
		</footer>
		<!-- .entry-footer -->
		<div>
			<?php $btn_text = get_theme_mod( 'blog_read_more_text', sailfish_theme()->customizer->get_default( 'blog_read_more_text' ) );
			$btn_visible    = $btn_text ? true : false;

			$utility->attributes->get_button( array(
					'visible' => $btn_visible,
					'class'   => 'link',
					'text'    => $btn_text,
					'icon'    => '<i class="linearicon linearicon-arrow-right"></i>',
					'html'    => '<a href="%1$s" %3$s><span class="link__text">%4$s</span>%5$s</a>',
					'echo'    => true,
			) );
			?>
		</div>
	</div><!-- .post-list__item-content -->

</article><!-- #post-## -->
