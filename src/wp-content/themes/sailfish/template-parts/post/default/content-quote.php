<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sailfish
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card' ); ?>>

	<?php $utility = sailfish_utility()->utility;
	$permalink     = $utility->satellite->get_post_permalink();
	$blog_featured_image = get_theme_mod( 'blog_featured_image', sailfish_theme()->customizer->get_default( 'blog_featured_image' ) );
	?>

	<?php if ( 'small' !== $blog_featured_image ) {
		get_template_part( 'template-parts/content-entry-meta-loop' );
	}; ?>

	<header class="entry-header">
		<?php sailfish_sticky_label();

		$title_html = ( is_single() ) ? '<h3 %1$s>%4$s</h3>' : '<h4 %1$s><a href="%2$s" rel="bookmark">%4$s</a></h4>';

		$utility->attributes->get_title( array(
				'class' => 'entry-title',
				'html'  => $title_html,
				'echo'  => true,
		) );
		?>
	</header><!-- .entry-header -->
	<div class="post-list__item-content">
		<a class="post-featured-content post-quote" href="<?php echo esc_url( $permalink ); ?>">
			<?php do_action( 'cherry_post_format_quote' ); ?>
		</a>
	</div><!-- .post-list__item-content -->

</article><!-- #post-## -->
