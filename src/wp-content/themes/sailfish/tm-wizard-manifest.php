<?php
/**
 * Plugins configuration example.
 *
 * @var array
 */
$plugins = array(
		'cherry-testi' => array(
			'name'   => esc_html__( 'Cherry Testimonials', 'sailfish' ),
			'access' => 'skins',
		),
		'power-builder-integrator' => array(
			'name'   => esc_html__( 'Power Builder Integrator', 'sailfish' ),
			'source' => 'remote',
			'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/power-builder-integrator.zip',
			'access' => 'skins',
		),
		'power-builder' => array(
		  	'name'   => esc_html__( 'Power Builder', 'sailfish' ),
		  	'source' => 'remote',
		  	'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/power-builder-upd.zip',
		  	'access' => 'skins',
		 ),
		'cherry-data-importer' => array(
			'name'   => esc_html__( 'Cherry Data Importer', 'sailfish' ),
			'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
			'path'   => 'https://github.com/CherryFramework/cherry-data-importer/archive/master.zip',
			'access' => 'base',
		),
		'cherry-team-members' =>array(
			'name'   => esc_html__( 'Cherry Team Members', 'sailfish' ),
			'access' => 'skins',
		),
		'cherry-services-list' => array(
			'name'   => esc_html__( 'Cherry Services List', 'sailfish' ),
			'access' => 'skins',
		),
		'cherry-sidebars' => array(
			'name'   => esc_html__( 'Cherry Sidebars', 'sailfish' ),
			'access' => 'skins',
		),
		'cherry-search' => array(
			'name'   => esc_html__( 'Cherry Search', 'sailfish' ),
			'access' => 'skins',
		),
		'contact-form-7' => array(
			'name' => esc_html__( 'Contact Form 7', 'sailfish' ),
			'access' => 'skins',
		),
		'tm-photo-gallery' => array(
			'name'     =>  esc_html__( 'Tm Gallery', 'sailfish' ),
			'access' => 'skins',
		),
	);

/**
 * Skins configuration example
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'cherry-data-importer',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'cherry-testi',
				'power-builder',
				'power-builder-integrator',
				'cherry-team-members',
				'cherry-services-list',
				'cherry-sidebars',
				'tm-photo-gallery',
				'cherry-search',
				'contact-form-7',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_64023',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default/default-thumb.png',
			'name'  => esc_html__( 'SailFish', 'sailfish' ),
		),


	),
);

$texts = array(
	'theme-name' => 'HosePress '
);

